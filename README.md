# DP-OTD
DP-OTD is a MATLAB code collection for several helper functions for computing differentially-private orthogonal tensor decomposition. Currently it contains 6 functions:
* DProbustTPM - implementation function for differentially private robust TPM due Wang and Anandkumar 2016
* my_multilinear_tensor - function for computing multi-linear projection of a tensor onto some subspaces. For the mathematical explanation, see Anandkumar et al. 2014. Tensor Decomposition for Learning Latent Variable Models.
* myEigValCheck - function for checking a particular eigenvalue against a set of given eigenvalues
* myEigVecCheck - function for checking a particular eigenvector against a set of given eigenvectors
* mySymmTensor - function for creating a symmetric tensor from a list of values
* myVectorNoise - function for generating vector valued noise due Sarwate et al. 2011. Differentially private Empirical Risk Minimization.

## Contact

* Hafiz Imtiaz (hafiz.imtiaz@rutgers.edu)

## Dependencies

The codes are tested on MATLAB R2016a. The “tensorlab toolbox” is required for the functions.


## Downloading

You can download the repository at https://gitlab.com/hafizimtiaz/dp-otd.git, or using

```
$ git clone https://gitlab.com/hafizimtiaz/dp-otd.git
```

## Installation

Copy the functions in your working directory or add the directory containing the functions in MATLAB path.


## License

MIT license

## Acknowledgements

This development of this collection was supported by support from the
following sources:

* National Institutes of Health under award 1R01DA040487-01A1

Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of National Institutes of Health.
